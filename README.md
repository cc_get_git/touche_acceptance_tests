# Touche Gherkin

Touche Gherkin Automated Acceptance tests repository

An easy to use BDD Test Automation suite for the Touche Web Application.


Using
 [Moonraker](https://github.com/LateRoomsGroup/moonraker/)  
 [Yadda](https://github.com/acuminous/yadda)
 [Selenium-Webdriver](https://code.google.com/p/selenium/wiki/WebDriverJs)
 [Mocha](http://mochajs.org/)
 [Chai](http://chaijs.com/)


* [Install](#install)
* [Configure](#configure)
* [Run](#run-your-tests)
* [Features Covered](#features)
* [Saucelabs / Browserstack integration](#saucelabs--browserstack-integration)
* [Reporting](#reporting)
* [Page object reference](#page-object-reference)
* [TODO](#todo)

### features

The feature list
* login_touche.feature

### Install

Pre-Requisite : Node.js [https://nodejs.org/download/] and npm should be available in the Test agent/environment.

The framwork - Moonraker can be installed via [npm](https://www.npmjs.org/) - `$ npm install moonraker`, or add `moonraker` to your `package.json`.

### Configure

The Automation can be configured using a `config.json` file in the project root:

* `baseUrl`        - Your base url, page object urls will be relative to this.*
* `featuresDir`    - The path to your features directory.*
* `stepsDir`       - The path to your step definitions directory.*
* `resultsDir`     - The path you'd like your results output to. (Default: /results)
* `reporter`       - The reporter type you'd like Moonraker to use (more on this [below](#reporting)).
* `threads`        - The number of threads you'd like to run with. (Default: 1) : Tests will execute parallelly if value is more than 1.
* `tags`           - Optional: Comma seperated list of feature tags to catogorise tests.
* `testTimeout`    - The maximum test (scenario step) timeout before its marked as a fail (ms). (Default: 60000)
* `elementTimeout` - The maximum time selenium will continuously try to find an element on the page (ms). (Default: 3000)
* `browser`        - An object describing your browser [desired capabilities](https://code.google.com/p/selenium/wiki/DesiredCapabilities).*
* `seleniumServer` - Optional: Address of your remote selenium standalone server.
* `language`       - Optional: sets the language to use (default: English).


\* - Required.

The example configuration above assumes using Chrome directly, to connect to a remote selenium server just add your server address to your `config.json`:

`"seleniumServer": "http://127.0.0.1:4444/wd/hub"`.

You can use this to connect to cloud service providers like [Saucelabs](https://saucelabs.com/) and [Browserstack](https://www.browserstack.com/automate). Please see [below]() for example browser configurations.

You can also set which language to use, using `language`, if you intend to use non English feature & step definition files. A full list of supported languages is available [here](https://github.com/acuminous/yadda/tree/master/lib/localisation).

All of Moonraker's configuration options can be overridden when running your tests (see below) if you add command line args (e.g: `--baseUrl=http://www.example.com` or `--browser.browserName=phantomjs`) or have set environment variables. They will take preference over the `config.json`, in that order - command line args > env vars > config.

### Run your tests

`npm install` To extract the latest node modules
`npm test` from within the example directory to run the entire test suite.

### Writing your tests

Tests using Moonraker are written using [Yadda](https://github.com/acuminous/yadda), a BDD implementation very similar to [Cucumber](http://cukes.info/) and run using the [Mocha](http://visionmedia.github.io/mocha/) JavaScript test framework.


### Components

Components are exactly like page objects and allow you to group elements together into a component, then add that component itself to a page object.

### Assertions

The 'should' style of the [Chai](http://chaijs.com/guide/styles/) assertion library is used in your step definitions.


### Saucelabs / Browserstack integration

Tests can be configured to run on cloud service providers like [Saucelabs](https://saucelabs.com/) and [Browserstack](https://www.browserstack.com/automate)
Config setup information is available at : https://www.npmjs.com/package/moonraker


### Reporting

As the tests are run using Mocha, you can use any of Mocha's [reporters](http://mochajs.org/#reporters).
Just set the required reporter in the config.
As Mocha is designed to run serially though you will experience issues when running Moonraker in parallel, so Moonraker comes with its own custom reporter for Mocha.


### Page object reference

As the examples show, all interactions with page elements (and the underlying driver) are abstracted away in your page objects. When you create a page object you have various ways of attaching elements to it so they can be interacted with in your step definitions:


### TODO

Moonraker has not been active for the last year or so but as a lightweight fast UI test framework there its a good investment to fork and contribute to it
