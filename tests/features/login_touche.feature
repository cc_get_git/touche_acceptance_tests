Feature: Basic Login functionality and UI

Scenario: Verify Login to Touche on web via email is successful

    Given I have loaded the Touche website 
    When I select to login
    And I choose to use the email authentication with [user] and [password]
    Then I should see the Journal page
    And I logout

Where:
    user | password
    toucheuser@gmail.com | touche

Scenario: Verify Login for failed attempt

    Given I have loaded the Touche website 
    When I select to login
    And I choose to use the mobile authentication with [code] [number] and [password]
    Then I should see the failed login message

Where:
    code |	number	|	password
    +34 |	619229736 |	 touch

Scenario: Verify Login to Touche on web via mobile is successful

    Given I have loaded the Touche website 
    When I select to login
    And I choose to use the mobile authentication with [code] [number] and [password]
    Then I should see the Journal page
    And I should see the payment 9.90 done at Touché BCN on tuesday the date jan 31
    And I logout

Where:
    code |  number  |   password
    +34 |   619229736 |  touche   
