var homePage = require('../pages/home'),
    journalPage = require('../pages/journal-page');


exports.define = function (steps) {

  steps.given("I have loaded the Touche website", function () {
    homePage.visit();
  });

  steps.when("I select to login", function () {
    homePage.linkLogin.isDisplayed();
    homePage.linkLogin.click();
  });

  steps.when("I choose to use the mobile authentication with $code $number and $password", function (code,number,password) {
    homePage.buttonLogin.isDisplayed();
    homePage.buttonLogin.click(); 
    homePage.radioPhone.isDisplayed();
    homePage.radioPhone.click(); 
    homePage.inputTxtPhone.sendKeys(code);
    homePage.inputTxtPhone.clear();
    homePage.inputTxtPhone.sendKeys(number);
    homePage.inputTxtPassword.sendKeys(password);
    homePage.buttonLoginSubmit.click();
  });

  steps.when("I choose to use the email authentication with $user and $password", function (user,password) {
    homePage.buttonLogin.isDisplayed();
    homePage.buttonLogin.click(); 
    homePage.radioEmail.isDisplayed();
    homePage.radioEmail.click(); 
    homePage.inputTxtEmail.sendKeys(user);
    homePage.inputTxtPassword.sendKeys(password);
    homePage.buttonLoginSubmit.click();
  });
  
  steps.then("I should see the failed login message", function () {
    homePage.txtMsgLoginFail.getText().then(function (text){
      text.should.have.string("The user does not exists or the password does not match");
    });
  });

  steps.then("I logout", function () {
    homePage.linkProfileMenu.click();
    homePage.headerLogout();
  });

};
