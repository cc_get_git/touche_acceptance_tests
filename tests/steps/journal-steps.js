var homePage = require('../pages/home'),
    journalPage = require('../pages/journal-page');

exports.define = function (steps) {
  
  steps.then("I should see the Journal page", function () {
    journalPage.headerJournalInfo.isDisplayed();
  });

  steps.then("I should see the payment $total done at $venue on $day the date $mmmdd", function (total,venue,day,mmmdd) {
    journalPage.listPayment.isDisplayed();
    journalPage.listPaymentDay.getText().then(function (text){
      text.should.have.string(day);
    });
    journalPage.listPaymentMMMDD.getText().then(function (text){
      text.should.have.string(mmmdd);
    });
    journalPage.listPaymentTotal.getText().then(function (text){
      text.should.have.string(total);
    });
    journalPage.listPaymentVenue.getText().then(function (text){
      text.should.have.string(venue);
    });
  });

};
