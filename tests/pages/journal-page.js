var Page = require('moonraker').Page,
    config = require('moonraker').config;

module.exports = new Page({

  headerJournalInfo: { get: function () {return this.element("//div[@class='current-page__info']//span['Journal']","xpath"); } },

  listPayment: { get: function () { return this.element("li.experiencesModule__listItem:nth-of-type(1)"); } },

  listPaymentDay: { get: function () { return this.element("li.experiencesModule__listItem:nth-of-type(1)>ul>li>div>.whenCell__firstLine>time"); } },

  listPaymentMMMDD: { get: function () { return this.element("li.experiencesModule__listItem:nth-of-type(1)>ul>li>div>.whenCell__secondLine>time"); } },

  listPaymentVenue: { get: function () { return this.element("li.experiencesModule__listItem:nth-of-type(1)>ul>li>div>.descriptionCell__h4"); } },

  listPaymentTotal: { get: function () { return this.element("li.experiencesModule__listItem:nth-of-type(1)>ul>li>.expsGrid__amountCell"); } },

});
