var Page = require('moonraker').Page;

module.exports = new Page({

  url: { value: '/#' },

  re: { get: function () { return this.element(".btn-primary"); } },

  linkLogo: { get: function () { return this.element("//h1/a[@href='/'][contains(text(),'MarsAir')]","xpath"); } },
  linkLogin: { get: function () { return this.element("//header//a[@class='touche-button']","xpath"); } },
  
  chooseLogin: { value: function (query) {
    var _this = this;
    this.waitFor(function () {
      return _this.buttonLoginSubmit.isDisplayed();
    }, 5000);
  } },

  linkLogout: { get: function () { return this.element("//a[text()='Logout']","xpath"); } },
  linkProfileMenu: { get: function () { return this.element(".userInfo__menuButton"); } },
  
  headerLogout: { value: function () {
    var _this = this;
    this.waitFor(function () {
      return _this.linkLogout.isDisplayed();
    }, 5000);
    this.linkLogout.click();
  }},
  
  buttonLogin: { get: function () { return this.element("//button[text()='Login']","xpath"); } },
  buttonLoginSubmit: { get: function () { return this.element(".loginPopUp__steps__form__button"); } },
  
  radioPhone: { get: function () { return this.element(".phone-item"); } }, 
  radioEmail: { get: function () { return this.element(".email-item"); } }, 

  inputTxtPhone: { get: function () { return this.element("[name='phone']"); } },
  inputTxtPassword: { get: function () { return this.element("[name='password']"); } },
  inputTxtEmail: { get: function () { return this.element("[name='email']"); } },
  
  txtMsgLoginFail : { get: function () { return this.element(".loginPopUp__footerText"); } },
  
});
